import { Program, Provider, setProvider, web3 } from '@project-serum/anchor';
import { PublicKey } from '@solana/web3.js';
import { AnchorService } from './anchor.service';
import { PROGRAM_IDL_FILE_PATH, PROGRAM_KEYPAIR_FILE_PATH } from './config';
import { ConfigService } from './config.service';
import { SolanaService } from './solana.service';

async function main() {
  const rpcUrl = await ConfigService.getRpcUrl()
  const connection = new web3.Connection(rpcUrl, 'confirmed')

  const defaultAccount = await ConfigService.getSigningAccount()
  const programAccount = await ConfigService.readAccountFromFile(PROGRAM_KEYPAIR_FILE_PATH)

  await SolanaService.getAccountBalance(connection, defaultAccount.publicKey)
  if (!(await SolanaService.isProgramAccount(connection, programAccount.publicKey))) {
    return
  }

  setProvider(Provider.local(rpcUrl))
  const program = await AnchorService.loadProgram(PROGRAM_IDL_FILE_PATH, programAccount.publicKey)
  const walletAccount = await SolanaService.generateKeypairFromSeed(programAccount.publicKey, '', programAccount.publicKey)

  const wallet = await printMultisgWalletInfo(program, walletAccount.publicKey)
  const requestAccount = await SolanaService.generateKeypairFromSeed(walletAccount.publicKey, `tx_${wallet.requestId}`, program.programId)
  await printCurrentRequestInfo(program, requestAccount.publicKey)
}

async function printMultisgWalletInfo(program: Program, address: PublicKey
): Promise<any> {
  const multisig = await program.account.multisigWallet.fetch(address) as any
  console.log('/-- BEGIN MULTISIG INFO --/')
  console.log('Nonce: ', multisig.nonce)
  console.log('Creator: ', multisig.creator.toBase58())
  console.log('Owners: ', (multisig.owners as PublicKey[]).map(owner => owner.toBase58()))
  console.log('Vote powers: ', multisig.votePowers)
  console.log('Required vote: ', multisig.requiredVote)
  console.log('Total vote: ', multisig.totalVote)
  console.log('Request ID: ', multisig.requestId)
  console.log('/-- END MULTISIG INFO --/\n')
  return multisig
}

async function printCurrentRequestInfo(program: Program, address: PublicKey) {
  const request = await program.account.request.fetch(address) as any
  console.log('/-- BEGIN REQUEST INFO --/')
  console.log('Request ID: ', request.id)
  console.log('Wallet: ', request.wallet.toBase58())
  console.log('Creator: ', request.creator.toBase58())
  console.log('Destination: ', request.destination.toBase58())
  console.log('Accounts: ', (request.accounts as web3.AccountMeta[]).map(acc => <Object>{ pubkey: acc.pubkey.toBase58(), isSigner: acc.isSigner, isWritable: acc.isWritable }))
  console.log('Data: ', request.data.toString('hex'))
  console.log('Voters: ', request.voteOwners)
  console.log('Vote Progress: ', request.voteProgress)
  console.log('Executed: ', request.isExecuted)
  console.log('Cancelled: ', request.isCancelled)
  console.log('/-- END REQUEST INFO --/\n')
}

main()
