import fs from 'mz/fs'

export class FileSystemService {
  static async readFromFile(path: string) {
    return fs.readFile(path, { encoding: 'utf8' })
  }
}
