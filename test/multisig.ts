import { Program, Provider, setProvider, web3 } from '@project-serum/anchor';
import { AnchorService } from './anchor.service';
import { PROGRAM_IDL_FILE_PATH, PROGRAM_KEYPAIR_FILE_PATH } from './config';
import { ConfigService } from './config.service';
import { SolanaService } from './solana.service';

async function initTestAccounts() : Promise<web3.Keypair[]> {
  const accounts = new Array<web3.Keypair>()
  for(let i = 0; i < 10; i++) {
    const path = `./test/id${i}.json`
    try {
      const account = await ConfigService.readAccountFromFile(path)
      accounts.push(account)
    }
    catch {
      const newAccount = web3.Keypair.generate()
      await ConfigService.writeAccountToFile(path, newAccount)
      accounts.push(newAccount)
    }
  }

  return accounts
}

async function main() {
  const rpcUrl = await ConfigService.getRpcUrl()
  const connection = new web3.Connection(rpcUrl, 'confirmed')

  const defaultAccount = await ConfigService.getSigningAccount()
  const programAccount = await ConfigService.readAccountFromFile(PROGRAM_KEYPAIR_FILE_PATH)
  const testAccounts = await initTestAccounts()

  await SolanaService.getAccountBalance(connection, defaultAccount.publicKey)
  if (!(await SolanaService.isProgramAccount(connection, programAccount.publicKey))) {
    return
  }

  setProvider(Provider.local(rpcUrl))
  const program = await AnchorService.loadProgram(PROGRAM_IDL_FILE_PATH, programAccount.publicKey)

  // Create new Multisig Wallet Instance
  const owners = [
    testAccounts[0].publicKey,
    testAccounts[1].publicKey,
    testAccounts[2].publicKey,
    testAccounts[3].publicKey,
    testAccounts[4].publicKey,
  ]
  const votePowers = Int16Array.from([1,2,3,4,5,])
  await foo(program)
  const walletAccount = await createDeterministicWallet(connection, program, owners, votePowers, 5)
  const [walletSigningAddress,] = await SolanaService.getSigningAddress(walletAccount.publicKey, program.programId)
  await airdropWallet(connection, defaultAccount, walletSigningAddress)

  // Create new Transaction
  const requestAccount = await createTransferRequest(program, walletAccount, testAccounts[0])

  // Vote
  await vote(program, walletAccount.publicKey, requestAccount.publicKey, testAccounts[2])
  await vote(program, walletAccount.publicKey, requestAccount.publicKey, testAccounts[4])

  // Execute
  await executeTransferRequest(program, walletAccount.publicKey, requestAccount.publicKey)
}

async function foo(program: Program,
) {
  await program.rpc.foo()
  console.log('/* foo invoked */')
}

async function createNewWallet(program: Program,
  owners: web3.PublicKey[],
  votePowers: Int16Array,
  requiredVote: number,
) {
  const walletAccount = web3.Keypair.generate()
  await program.rpc.createNewWallet(owners[0], owners, votePowers, requiredVote, {
    accounts: {
      wallet: walletAccount.publicKey,
      rent: web3.SYSVAR_RENT_PUBKEY,
    },
    signers: [walletAccount],
    instructions: [
      await program.account.multisigWallet.createInstruction(walletAccount, 10000),
    ],
  })
  console.log('/* createNewWallet invoked */')
  console.log('Wallet address: ', walletAccount.publicKey.toBase58())
}

async function createDeterministicWallet(connection: web3.Connection,
  program: Program,
  owners: web3.PublicKey[],
  votePowers: Int16Array,
  requiredVote: number,
): Promise<web3.Keypair> {
  const walletAccount = await SolanaService.generateKeypairFromSeed(program.programId, '', program.programId)
  const [, nonce] = await SolanaService.getSigningAddress(walletAccount.publicKey, program.programId)
  if (!(await SolanaService.isAddressInUse(connection, walletAccount.publicKey))) {
    await program.rpc.createNewWallet(nonce, owners[0], owners, votePowers, requiredVote, {
      accounts: {
        wallet: walletAccount.publicKey,
        rent: web3.SYSVAR_RENT_PUBKEY,
      },
      signers: [walletAccount],
      instructions: [
        await program.account.multisigWallet.createInstruction(walletAccount, 1000),
      ],
    })
    console.log('/* createNewWallet invoked */')
  }
  console.log('Wallet address: ', walletAccount.publicKey.toBase58())
  return walletAccount
}

async function airdropWallet(connection: web3.Connection, owner: web3.Keypair, walletAddress: web3.PublicKey
) {
  const transaction: web3.Transaction = new web3.Transaction()
  const instruction: web3.TransactionInstruction = web3.SystemProgram.transfer(<web3.TransferParams>{
    fromPubkey: owner.publicKey,
    toPubkey: walletAddress,
    lamports: 500000000
  })
  transaction.add(instruction)
  await web3.sendAndConfirmTransaction(connection, transaction, [owner])
  console.log(`Transfered to ${walletAddress.toBase58()} 500,000,000 lamports`)
}

async function createRequest(program: Program,
  walletAccount: web3.Keypair,
  ownerAccount: web3.Keypair,
  txDestination: web3.PublicKey,
  txAccounts: web3.AccountMeta[],
  txData: Buffer,
): Promise<web3.Keypair> {
  const multisig = await program.account.multisigWallet.fetch(walletAccount.publicKey) as any
  const requestAccount = await SolanaService.generateKeypairFromSeed(walletAccount.publicKey, `tx_${multisig.requestId + 1}`, program.programId)
  await program.rpc.createNewRequest(txDestination, txAccounts, txData, {
    accounts: {
      wallet: walletAccount.publicKey,
      request: requestAccount.publicKey,
      owner: ownerAccount.publicKey,
      rent: web3.SYSVAR_RENT_PUBKEY,
    },
    signers: [requestAccount, ownerAccount],
    instructions: [
      await program.account.request.createInstruction(requestAccount, 1000),
    ],
  })
  console.log('/* create_new_request invoked */')
  console.log('Request address: ', requestAccount.publicKey.toBase58())
  return requestAccount
}

async function createFooRequest(program: Program, walletAccount: web3.Keypair, ownerAccount: web3.Keypair
): Promise<web3.Keypair> {
  const data = program.coder.instruction.encode('foo', {}) // because foo doesn't need additional account, so we put [] in the args
  return createRequest(program, walletAccount, ownerAccount, program.programId, [], data)
}

async function createFooWithFriendRequest(program: Program, walletAccount: web3.Keypair, ownerAccount: web3.Keypair, friendAccount: web3.Keypair,
): Promise<web3.Keypair> {
  const data = program.coder.instruction.encode('foo_with_friend', {
    amount: [1, 2, 3, 4, 5],
    delegate: [9, 8, 7, 6],
  })
  const signers: web3.AccountMeta[] = [
    <web3.AccountMeta> {
      pubkey: friendAccount.publicKey,
      isWritable: false,
      isSigner: true
    }
  ]
  return createRequest(program, walletAccount, ownerAccount, program.programId, signers, data)
}

async function createChangeOwnersRequest(program: Program, walletAccount: web3.Keypair, ownerAccount: web3.Keypair
): Promise<web3.Keypair> {
  const testAccounts = await initTestAccounts()
  const owners = [
    testAccounts[0].publicKey,
    testAccounts[2].publicKey,
    testAccounts[4].publicKey,
    testAccounts[6].publicKey,
    testAccounts[8].publicKey,
  ]
  const votePowers = Int16Array.from([5,4,3,2,1,])
  const [walletSigningAddress,] = await SolanaService.getSigningAddress(walletAccount.publicKey, program.programId)
  const signers: web3.AccountMeta[] = [
    <web3.AccountMeta> {
      pubkey: walletAccount.publicKey,
      isWritable: true,
      isSigner: true
    },
    <web3.AccountMeta> {
      pubkey: walletSigningAddress,
      isWritable: false,
      isSigner: true
    }
  ]
  const data = program.coder.instruction.encode('change_owners', {
    owners,
    votePowers,
    requiredVote: 7
  })
  return createRequest(program, walletAccount, ownerAccount, program.programId, signers, data)
}

async function createTransferRequest(program: Program, walletAccount: web3.Keypair, ownerAccount: web3.Keypair
): Promise<web3.Keypair> {
  const testAccounts = await initTestAccounts()
  const [walletSigningAddress,] = await SolanaService.getSigningAddress(walletAccount.publicKey, program.programId)
  const data = web3.SystemProgram.transfer(<web3.TransferParams>{
    fromPubkey: walletSigningAddress,
    toPubkey: testAccounts[1].publicKey,
    lamports: 100000000
  }).data
  const requestAccounts: web3.AccountMeta[] = [
    <web3.AccountMeta> {
      pubkey: walletSigningAddress,
      isWritable: true,
      isSigner: true
    },
    <web3.AccountMeta> {
      pubkey: web3.SystemProgram.programId,
      isWritable: false,
      isSigner: false
    },
  ]
  return createRequest(program, walletAccount, ownerAccount, web3.SystemProgram.programId, requestAccounts, data)
}

async function executeFooWithFriendRequest(program: Program, walletAddress: web3.PublicKey, requestAddress: web3.PublicKey,
) {
  const testAccounts = await initTestAccounts()
  const requestAccounts : web3.AccountMeta[] = [
    <web3.AccountMeta> {
      pubkey: testAccounts[1].publicKey,
      isSigner: true,
      isWritable: false,
    }
  ]
  const requestSigners = [testAccounts[1]]
  await executeRequest(program, walletAddress, requestAddress, requestAccounts, requestSigners)
}

async function executeChangeOwnersRequest(program: Program, walletAddress: web3.PublicKey, requestAddress: web3.PublicKey,
) {
  const walletAccount = await SolanaService.generateKeypairFromSeed(program.programId, '', program.programId)
  const [walletSigningAddress,] = await SolanaService.getSigningAddress(walletAddress, program.programId)
  const requestAccounts : web3.AccountMeta[] = [
    <web3.AccountMeta> {
      pubkey: walletAddress,
      isWritable: true,
      isSigner: true
    },
    <web3.AccountMeta> {
      pubkey: walletSigningAddress,
      isWritable: false,
      isSigner: false
    }
  ]
  const requestSigners = [walletAccount]
  await executeRequest(program, walletAddress, requestAddress, requestAccounts, requestSigners)
}

async function executeTransferRequest(program: Program, walletAddress: web3.PublicKey, requestAddress: web3.PublicKey,
  ) {
    const walletAccount = await SolanaService.generateKeypairFromSeed(program.programId, '', program.programId)
    const [walletSigningAddress,] = await SolanaService.getSigningAddress(walletAddress, program.programId)
    const testAccounts = await initTestAccounts()
    const requestAccounts: web3.AccountMeta[] = [
      <web3.AccountMeta> {
        pubkey: walletAccount.publicKey,
        isWritable: false,
        isSigner: true
      },
      <web3.AccountMeta> {
        pubkey: walletSigningAddress,
        isWritable: true,
        isSigner: false
      },
      <web3.AccountMeta> {
        pubkey: testAccounts[1].publicKey,
        isWritable: true,
        isSigner: false
      },
      <web3.AccountMeta> {
        pubkey: web3.SystemProgram.programId,
        isWritable: false,
        isSigner: false
      },
    ]
    const requestSigners = [walletAccount]
    await executeRequest(program, walletAddress, requestAddress, requestAccounts, requestSigners)
  }

async function vote(program: Program,
  walletAddress: web3.PublicKey,
  requestAddress: web3.PublicKey,
  ownerAccount: web3.Keypair,
) {
  await program.rpc.vote({
    accounts: {
      wallet: walletAddress,
      request: requestAddress,
      owner: ownerAccount.publicKey,
    },
    signers: [ownerAccount],
  })
  console.log('/* vote invoked */')
  console.log(`User ${ownerAccount.publicKey.toBase58()} voted`)
}

async function executeRequest(program: Program,
  walletAddress: web3.PublicKey,
  requestAddress: web3.PublicKey,
  requestAccounts: web3.AccountMeta[],
  requestSigners: web3.Keypair[],
) {
  const [walletSigningAddress,] = await SolanaService.getSigningAddress(walletAddress, program.programId)
  const remainingAccounts = requestAccounts
    .concat({
      pubkey: program.programId,
      isWritable: false,
      isSigner: false,
    })
  const signers = requestSigners
  await program.rpc.executeRequest({
    accounts: {
      wallet: walletAddress,
      walletSigner: walletSigningAddress,
      request: requestAddress,
    },
    remainingAccounts,
    signers
  })

  console.log('/* execute_request invoked */')
  console.log('Request address: ', requestAddress.toBase58())
}

main()
