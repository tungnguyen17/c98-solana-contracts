use anchor_lang::prelude::*;
use anchor_lang::solana_program;
use anchor_lang::solana_program::instruction::Instruction;

#[program]
mod coin98_multisig {
  use super::*;

  pub fn foo(_ctx: Context<FooContext>,
  ) -> ProgramResult {
    msg!("foo invoked");
    Ok(())
  }

  pub fn foo_with_friend(_ctx: Context<FooWithFriendContext>,
    _amount: Vec<u16>,
    _delegate: Vec<u32>,
  ) -> ProgramResult {
    msg!("foo_with_friend invoked");
    Ok(())
  }

  pub fn create_new_wallet(ctx: Context<CreateWalletContext>,
    nonce: u8,
    creator: Pubkey,
    owners: Vec<Pubkey>,
    vote_powers: Vec<u16>,
    required_vote: u16,
  ) -> ProgramResult {
    msg!("crate_new_wallet invoked");
    let wallet = &mut ctx.accounts.wallet;
    wallet.nonce = nonce;
    wallet.creator = creator;
    wallet.owners = owners;
    wallet.vote_powers = vote_powers;
    wallet.required_vote = required_vote;
    wallet.total_vote = wallet.owners.len() as u16;
    wallet.request_id = 0;
    Ok(())
  }

  pub fn create_new_request(ctx: Context<CreateRequestContext>,
    destination: Pubkey,
    accs: Vec<TransactionAccount>,
    data: Vec<u8>,
  ) -> ProgramResult {
    msg!("create_new_request invoked");

    let owner_index = ctx.accounts.wallet.owners
      .iter()
      .position(|a| a == ctx.accounts.owner.key)
      .ok_or(ErrorCode::InvalidOwner)?;

    let wallet = &mut ctx.accounts.wallet;
    wallet.request_id = wallet.request_id + 1;
    let request = &mut ctx.accounts.request;
    request.id = wallet.request_id;
    request.wallet = *ctx.accounts.wallet.to_account_info().key;
    request.creator = *ctx.accounts.owner.key;
    request.destination = destination;
    request.accounts = accs;
    request.data = data;

    let mut voted_owners = Vec::new();
    voted_owners.resize(ctx.accounts.wallet.owners.len(), false);
    voted_owners[owner_index] = true;
    request.vote_owners = voted_owners;
    request.vote_progress = ctx.accounts.wallet.vote_powers[owner_index];
    Ok(())
  }

  pub fn vote(ctx: Context<VoteContext>,
  ) -> ProgramResult {
    msg!("vote invoked");
    let owner_index = ctx.accounts.wallet.owners
      .iter()
      .position(|a| a == ctx.accounts.owner.key)
      .ok_or(ErrorCode::InvalidOwner)?;

    let request = &mut ctx.accounts.request;
    request.vote_owners[owner_index] = true;
    request.vote_progress += ctx.accounts.wallet.vote_powers[owner_index];
    Ok(())
  }

  pub fn execute_request(ctx: Context<ExecuteRequestContext>,
  ) -> ProgramResult {
    msg!("execute_request invoked");

    let mut instruction: Instruction = (&*ctx.accounts.request).into();
    instruction.accounts = instruction.accounts
      .iter()
      .map(|acc| {
        if &acc.pubkey == ctx.accounts.wallet_signer.key {
          AccountMeta::new(acc.pubkey, true)
        } else {
          acc.clone()
        }
      })
      .collect();
    let seeds = &[
      ctx.accounts.wallet.to_account_info().key.as_ref(),
      &[ctx.accounts.wallet.nonce],
    ];
    let signer = &[&seeds[..]];
    let accounts = ctx.remaining_accounts;
    ctx.accounts.request.is_executed = true;
    solana_program::program::invoke_signed(&instruction, &accounts, signer)?;
    Ok(())
  }

  pub fn change_owners(ctx: Context<ChangeOwnerContext>,
    owners: Vec<Pubkey>,
    vote_powers: Vec<u16>,
    required_vote: u16,
  ) -> ProgramResult {
    msg!("change_owners invoked");
    let wallet = &mut ctx.accounts.wallet;
    wallet.owners = owners;
    wallet.vote_powers = vote_powers;
    wallet.required_vote = required_vote;
    wallet.total_vote = wallet.owners.len() as u16;
    Ok(())
  }
}

#[derive(Accounts)]
pub struct FooContext {
}

#[derive(Accounts)]
pub struct FooWithFriendContext<'info> {
  #[account(signer)]
  pub friend: AccountInfo<'info>,
}

#[derive(Accounts)]
pub struct CreateWalletContext<'info> {
  #[account(init)]
  pub wallet: ProgramAccount<'info, MultisigWallet>,
  pub rent: Sysvar<'info, Rent>,
}

#[derive(Accounts)]
pub struct CreateRequestContext<'info> {
  #[account(mut)]
  pub wallet: ProgramAccount<'info, MultisigWallet>,
  #[account(init)]
  pub request: ProgramAccount<'info, Request>,
  #[account(signer)]
  pub owner: AccountInfo<'info>,
  pub rent: Sysvar<'info, Rent>,
}

#[derive(Accounts)]
pub struct VoteContext<'info> {
  pub wallet: ProgramAccount<'info, MultisigWallet>,
  #[account(mut)]
  pub request: ProgramAccount<'info, Request>,
  #[account(signer)]
  pub owner: AccountInfo<'info>,
}

#[derive(Accounts)]
pub struct ExecuteRequestContext<'info> {
  wallet: ProgramAccount<'info, MultisigWallet>,
  #[account(seeds = [
    wallet.to_account_info().key.as_ref(),
    &[wallet.nonce],
  ])]
  wallet_signer: AccountInfo<'info>,
  #[account(mut, belongs_to = wallet)]
  request: ProgramAccount<'info, Request>,
}

#[derive(Accounts)]
pub struct ChangeOwnerContext<'info> {
  #[account(mut)]
  wallet: ProgramAccount<'info, MultisigWallet>,
  #[account(seeds = [
    wallet.to_account_info().key.as_ref(),
    &[wallet.nonce],
  ])]
  wallet_signer: AccountInfo<'info>,
}

#[account]
pub struct MultisigWallet {
  pub nonce: u8,
  pub creator: Pubkey,
  pub owners: Vec<Pubkey>,
  pub vote_powers: Vec<u16>,
  pub required_vote: u16,
  pub total_vote: u16,
  pub request_pending: bool,
  pub request_id: u32,
}

#[account]
pub struct Request {
  pub id: u32,
  pub wallet: Pubkey,
  pub creator: Pubkey,
  pub destination: Pubkey,
  pub accounts: Vec<TransactionAccount>,
  pub data: Vec<u8>,
  pub vote_owners: Vec<bool>,
  pub vote_progress: u16,
  pub is_executed: bool,
  pub is_cancelled: bool,
}

// mimic AccountMeta
#[derive(AnchorSerialize, AnchorDeserialize, Clone)]
pub struct TransactionAccount {
  pubkey: Pubkey,
  is_signer: bool,
  is_writable: bool,
}

impl From<&Request> for Instruction {
  fn from(tx: &Request
  ) -> Instruction {
    Instruction {
      program_id: tx.destination,
      accounts: tx.accounts.clone().into_iter().map(Into::into).collect(),
      data: tx.data.clone(),
    }
  }
}

impl From<TransactionAccount> for AccountMeta {
  fn from(account: TransactionAccount)
  -> AccountMeta {
    match account.is_writable {
      false => AccountMeta::new_readonly(account.pubkey, account.is_signer),
      true => AccountMeta::new(account.pubkey, account.is_signer),
    }
  }
}

#[error]
pub enum ErrorCode {
    #[msg("C98MSiG: Not an owner.")]
    InvalidOwner,
}
